Thank you for purchasing this Road Material.

Simple to use substance.

Play with each field to see what happens.  You can have many different road markings, choose the colour and more.

Further down you will see the asphalt section. tweaking a these can give amazing results and create completley different kinds of road texture.

Also, simply drop the RoadMatUpdater script onto the object that has one of the substance materials on it and you can set different snow, water, ice and moss levels at run time.


Simply reference the RoadMatUpdater script in your own script, and set any of the following variables:

increaseSnow =false;
decreaseSnow =false;
float maxAutoSnow = 0.5f; ( 0.0f = none -> 1.0f = full )

increaseWater =false;
decreaseWater =false;
float maxAutoWater = 0.5f; ( 0.0f = none -> 1.0f = full )

increaseIce = false;
decreaseIce = false;
float maxAutoIce = 0.5f; ( 0.0f = none -> 1.0f = full )

increaseMoss = false;
decreaseMoss = false;
float maxAutoMoss = 0.5f; ( 0.0f = none -> 1.0f = full )