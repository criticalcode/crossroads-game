﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class RoadMatUpdater : MonoBehaviour {
	private ProceduralPropertyDescription [] PPD;
	public bool LiveUpdate = false;

	public ProceduralMaterial PM;
	public string[] ListOfTypes;
	
	public bool increaseSnow =false;
	public bool decreaseSnow =false;
	public float maxAutoSnow = 0.5f;
		
		
		public bool increaseWater =false;
	public bool decreaseWater =false;
	public float maxAutoWater = 0.5f;


    public bool increaseIce = false;
    public bool decreaseIce = false;
    public float maxAutoIce = 0.5f;

    public bool increaseMoss = false;
    public bool decreaseMoss = false;
    public float maxAutoMoss = 0.5f;


    public int SnowLabel = 11;
    public int WaterLabel = 8;
    public int IceLabel = 10;
    public int MossLabel = 12;



    public float WaterLevel = 0.0f;

    public float IceLevel = 0.0f;

    public float SnowLevel = 0.0f;

    public float MossLevel = 0.0f;
		// Use this for initialization
    void Start()
    {

        PM = GetComponent<Renderer>().sharedMaterial as ProceduralMaterial;
        PPD = PM.GetProceduralPropertyDescriptions();

        ListOfTypes = new string[PPD.Length];

        int cnt = 0;
        while (cnt < PPD.Length)
        {
            ListOfTypes[cnt] = cnt.ToString() + " : " + PPD[cnt].name + " | " + PPD[cnt].type.ToString();

            if ( PPD[cnt].name.Contains( "Road_Surface_Effect_Water_Level" ))
            {
                WaterLabel = cnt;
            }
            cnt++;
        }
    }

    void OnEnable()
    {

        PM = GetComponent<Renderer>().sharedMaterial as ProceduralMaterial;
        PPD = PM.GetProceduralPropertyDescriptions();

        ListOfTypes = new string[PPD.Length];

        int cnt = 0;
        while (cnt < PPD.Length)
        {
            ListOfTypes[cnt] = cnt.ToString() + " : " + PPD[cnt].name + " | " + PPD[cnt].type.ToString();
            cnt++;
        }
    }


	void FixedUpdate () {

		if (increaseSnow == true && SnowLevel < maxAutoSnow) {
			
			SnowLevel+=0.01f;
		}else
		if (decreaseSnow == true&&!increaseSnow) {
			
			SnowLevel-=0.05f;
		}

        if (increaseWater == true && WaterLevel < maxAutoWater)
        {

            WaterLevel += 0.05f;
        }
        else
            if (decreaseWater == true && !increaseWater)
            {

                WaterLevel -= 0.05f;
            }

        if (increaseMoss == true && MossLevel < maxAutoMoss)
        {

            MossLevel += 0.05f;
        }
        else
            if (decreaseMoss == true && !increaseMoss)
            {

                MossLevel -= 0.05f;
            }
		
		
		
		if (increaseIce == true && IceLevel < maxAutoIce) {
			
			IceLevel+=0.05f;
		}else
		if (decreaseIce == true&&!increaseIce) {
			
			IceLevel-=0.05f;
		}

        if (WaterLevel < 0.0f)
        {
            WaterLevel = 0;
        }

        if (IceLevel < 0)
        {
            IceLevel = 0;
        }
        if (SnowLevel < 0)
        {
            SnowLevel = 0;
        }

        if (MossLevel < 0)
        {
            MossLevel = 0;
        }

		SetSnow (SnowLevel);
			SetWater (WaterLevel);
			SetIce (IceLevel);
            SetMoss(MossLevel);
            if (needRebuild)
            {
                PM.RebuildTextures();
                needRebuild = false;
            }
		}
    bool needRebuild = true;
    private void SetMoss(float lvl)
    {
        if (lvl > maxAutoMoss)
        {
            lvl = maxAutoMoss;

        }
        PM.SetProceduralFloat(PPD[MossLabel].name, lvl);
        if (lvl != PM.GetProceduralFloat(PPD[MossLabel].name))
        {
            IceLevel = PM.GetProceduralFloat(PPD[MossLabel].name);
            needRebuild = true;
        }
       // PM.RebuildTextures();
    }

	//float snowLevel = 0.0f;
	public void SetIce ( float lvl )
		
	{
		if (lvl > maxAutoIce) {
            lvl = maxAutoIce;
			
		}
		PM.SetProceduralFloat(PPD [IceLabel].name , lvl);
        if (lvl != PM.GetProceduralFloat(PPD[IceLabel].name))
        {
            IceLevel = PM.GetProceduralFloat(PPD[IceLabel].name);
            needRebuild = true;
        }
		//PM.RebuildTextures ();
	}
	

	
	public void SetWater ( float lvl )
		
	{
		if (lvl > maxAutoWater) {
			lvl = maxAutoWater;
			
		}
        PM.SetProceduralFloat("Road_Surface_Effect_Water_Level", lvl);
        if (lvl != PM.GetProceduralFloat("Road_Surface_Effect_Water_Level"))
        {
            WaterLevel = PM.GetProceduralFloat("Road_Surface_Effect_Water_Level");
            needRebuild = true;
        }
		//PM.RebuildTextures ();
	}
	



	public void SetSnow ( float lvl )
		
	{
		if (lvl > maxAutoSnow) {
			lvl = maxAutoSnow;

		}
        PM.SetProceduralFloat("Road_Surface_Effect_Snow", lvl);
        if (lvl != PM.GetProceduralFloat("Road_Surface_Effect_Snow"))
        {
            SnowLevel = PM.GetProceduralFloat("Road_Surface_Effect_Snow");
            needRebuild = true;
        }
		//PM.RebuildTextures ();
	}
	
	
}
